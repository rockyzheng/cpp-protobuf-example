#include <iostream>
#include <fstream>
#include "proto/message.pb.h"

int main(int argc, char const *argv[])
{

    message::Message m ;
    std::string out_name = "test.out.protobuf";
    std::fstream output(out_name, std::ios::out | std::ios::trunc | std::ios::binary);

//  m.set_id(0);
//  m.SerializeToOstream(&output);
//  std::cout << "Hello Proto!" << std::endl;

    message::Person person;
//    person.set_name("John Doe");
//    person.set_id(1234);
//    person.set_email("jdoe@example.com");
//    person.SerializeToOstream(&output);

    std::fstream input(out_name, std::ios::in | std::ios::binary);
    person.ParseFromIstream(&input);
//  std::cout << "Name: " << person.name << std::endl;
    return 0;

}
